import React from 'react'
import ReactDOM from 'react-dom'

function App() {
  return (
    <div>
      <h1>Gitlab to S3 - master branch</h1>
    </div>
  )
}

ReactDOM.render(<App />, document.querySelector('#app'))
